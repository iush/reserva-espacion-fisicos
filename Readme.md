## Introducción

Este proyecto busca formalizar el conocimiento de la infraestructura fisica, que permita garantizar las funciones universitarias, la inter y transdisciplinariedad, el pluralismo de saberes y conocimientos y el **bienestar de la comunidad universitaria**.

- Como imaginamos los espacios educativos?
- Como hacer sostenible la actual infraestructura y las futuras edificaciones?
- Como superar el deficit de espacios?
- Como mejorar los espacios actuales para las practicas pedagogicas con bienestar y calidad?
- Como gestionar el uso de los espacios?

## Definicion y objetivo

El sistema de Espacios Educativos, se construye como instrumento de planificacion y ordenamiento estrategico de los recursos fisicos de la Universidad, el sistema se basara inicialmente en la sede "El Estadio" pero debe poderse ampliar para incluir sedes adicionales.

El objetivo principal del Sistema, se contempla como el inventario general de la planta fisica (Salones, oficinas , salas de reuniones y auditorios). 

### Etapas:

### Etapa de preparacion:

- Identificacion de datos preexistentes e infraestructura instalada.
    - **Espacios Academicos**.
    - Espacios comerciales.
    - Servicios de bienestar.
    - Espacios administrativos.
    - Espacios de circulacion. 

- Identificacion de actores academicos que utilizan y consumen los recursos:
    - **Asignaturas**.
    - Conferencias y eventos.

- Evaluar la situación y ubicación geografica de la Universidad en la ciudad.
    - Ventajas y desventajas.
    - Oportunidades empresariales de su ubicacion.

### Etapa de desarrollo

- Establecer CI.

### Detalle de base de datos.

El desarrollo, pruebas y mantenimiento de la base de datos se realiza en un marco de desarrollo sobre R y python.
[Rpubs:Espacios-Educativos](http://rpubs.com/oemunoz/Espacios-Educativos)

### Etapa de analisis.

- Establecer el funcionamiento Institucional basado en un inventario de los espacios fisicos y factores asociados:
    - Tiempo de movilizacion.
    - Movilidad.
    - Medio ambiente.
    - Eficiencia energetica.
    - Tiempo efectivo de utilizacion.
    - Intencidad de utilizacion.
    - Utilizacion efectiva.
    - Utiliizacion deseada.

### Sub-Productos.

- Capacidad instalada
- Capacidad economica
- Capacidad disponible
- Capacidad tecnica

----
