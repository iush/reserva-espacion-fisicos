### Transaction model

Secuencia de transacciones

```mermaid
sequenceDiagram
    participant User
    participant Option_menu
    User->Option_menu: Create new room booking
    loop Healthcheck
        Option_menu->Option_menu: Add new/cancel booking/user/room
    end
    Note right of Booking_form: Add, Cancel User 
    Option_menu->Booking_form: Add/Remove
    Note right of User_form: Add, Cancel booking 
    Option_menu->User_form: Add/Remove
    Note right of Room_form: Add, Cancel Room
    Booking_form-->Database: Save registry
    Booking_form-->Database: Remove registry
    Database->Booking_form: Confirm booking
    User_form-->Database: Save registry
    User_form-->Database: Remove registry
    Database->User_form: Confirm user
    Room_form-->Database: Save registry
    Room_form-->Database: Remove registry
    Database->Room_form: Confirm user
```
